"use strict";

//@TODO: Rework/Rethink, not happy with that -.-

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Templater = function () {
    function Templater() {
        _classCallCheck(this, Templater);

        Stellar.Kernel.swig = require('swig');
        // Disable express cache to rely only on swig's one
        Stellar.Kernel.app.set('view cache', false);
        this.loadConfig();
        this.loadFilters();
    }

    _createClass(Templater, [{
        key: 'loadFilters',
        value: function loadFilters() {
            //@TODO: To be removed, for test purpose only ATM
            //Instead make something that load classes
            var mydump = function mydump(arr, level) {
                var dumped_text = "";
                if (!level) level = 0;

                var level_padding = "";
                for (var j = 0; j < level + 1; j++) {
                    level_padding += "    ";
                }if ((typeof arr === 'undefined' ? 'undefined' : _typeof(arr)) == 'object') {
                    for (var item in arr) {
                        var value = arr[item];

                        if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) == 'object') {
                            dumped_text += level_padding + "'" + item + "' ...\n";
                            dumped_text += mydump(value, level + 1);
                        } else {
                            dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                        }
                    }
                } else {
                    dumped_text = "===>" + arr + "<===(" + (typeof arr === 'undefined' ? 'undefined' : _typeof(arr)) + ")";
                }
                return dumped_text;
            };
            Stellar.Kernel.swig.setFilter('dump', mydump);
        }
    }, {
        key: 'loadConfig',
        value: function loadConfig() {
            Stellar.Kernel.swig.setDefaults({
                cache: Stellar.Config.__useCache,
                locals: Stellar.Config.__viewLocals
            });
        }
    }, {
        key: 'registerFromConfig',
        value: function registerFromConfig() {
            Stellar.Kernel.app.engine('tpl', Stellar.Kernel.swig.renderFile);
            Stellar.Kernel.app.set('view engine', 'tpl');
            Stellar.Kernel.app.set('views', Stellar.Config.rootPath + 'views');
        }
    }]);

    return Templater;
}();

module.exports = Templater;
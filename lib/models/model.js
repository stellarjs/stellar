"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var QueryBuilder = require('./../querybuilder/querybuilder');

var Model = function (_QueryBuilder) {
    _inherits(Model, _QueryBuilder);

    function Model() {
        _classCallCheck(this, Model);

        var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(Model).call(this));

        _this2.initModelMembers();
        //@TODO: Save children config if any
        // this.loadCustomConfig();

        // Be sure its the last thing we init in this constructor
        _this2.initalModelMembers = Object.getOwnPropertyNames(_this2);
        _this2.initalModelMembers.push('initalModelMembers');
        return _this2;
    }

    _createClass(Model, [{
        key: 'initModelMembers',
        value: function initModelMembers() {
            this.isNewObject = true;
            this.ignoreStatement = false;
            // Get Original Model Name
            this.table = this.constructor.name.toLowerCase();
            this.timestamp = false;
            this.primaryKey = 'id';
            this.guarded = [];
            this.fillable = [];
            this.connectionName = null;
            this.path = '';

            //@TODO: Segment each part of a possible Request (WHERE/UPDATE/FROM/JOIN/ORDER/GROUP/...) into independant var.
            // and once we have to execute query, just rebuild the full query depending on these variables
        }
    }, {
        key: 'all',
        value: function () {
            var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
                var result;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return this.select('*').from(this.table).get();

                            case 2:
                                result = _context.sent;

                                this.resetQueryString();
                                return _context.abrupt('return', result);

                            case 5:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function all() {
                return ref.apply(this, arguments);
            }

            return all;
        }()
    }, {
        key: 'save',
        value: function save() {
            var query = null;
            var columnValueMap = this.getModelAttributes();
            var _this = this;

            if (this.ignoreStatement === true) {
                query = this.insertIgnoreIn(this.table);
            } else {
                query = this.insertIn(this.table);
            }

            var columnValueMapLength = columnValueMap.length - 1;

            Stellar._.each(columnValueMap, function (rowData, key) {
                var rowValue = _this.mysql.escape(rowData.rowValue);
                var colName = _this.mysql.escape(rowData.columnName).replace(/'/g, '');
                if (key == columnValueMapLength) {
                    query.addRowAssignation(rowValue, colName, true);
                } else {
                    query.addRowAssignation(rowValue, colName);
                }
            });

            query.executeQuery();
            this.resetQueryString();
        }

        // @TODO: Test + Rework

    }, {
        key: 'find',
        value: function () {
            var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(_id) {
                var result;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return this.select('*').from(this.table).where(this.primaryKey, '=', _id).get();

                            case 2:
                                result = _context2.sent;

                                this.resetQueryString();
                                // Return [0] since find should always return a single response, not a collection
                                return _context2.abrupt('return', result[0]);

                            case 5:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function find(_x) {
                return ref.apply(this, arguments);
            }

            return find;
        }()
    }, {
        key: 'get',
        value: function () {
            var ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee3() {
                var queryResult;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return this.executeQuery();

                            case 2:
                                queryResult = _context3.sent;
                                return _context3.abrupt('return', queryResult);

                            case 4:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function get() {
                return ref.apply(this, arguments);
            }

            return get;
        }()
    }, {
        key: 'rebuildToNativeObject',

        //var finalResult = false;
        //var _this = this;
        /*
                console.log('Just got result of query', new Date.now());
        
                if (queryResult.length > 1) {
                    finalResult = [];
                    Stellar._.each(queryResult, function(row, key){
                        // Rebuild Original Object;
                        var objectModel = _this.rebuildToNativeObject(row);
                        finalResult.push(objectModel);
                    });
                } else if (queryResult.length == 1) {
                    finalResult = this.rebuildToNativeObject(queryResult[0]);
                } else {
                    throw new Error('An error occured while fetching data from DB');
                }
                console.log('PROUT PROUT');
                console.log('FINAL RESULT', finalResult);
                return finalResult;*/
        value: function rebuildToNativeObject(row) {
            var finalObject = {};
            var finalObject = new (require(Stellar.Config.modelsPath + this.path + this.constructor.name))();
            Stellar._.each(row, function (prop, keyName) {
                finalObject[keyName] = prop;
            });
            return finalObject;
        }

        /*
        * Interactions with DB methods goes through queryBuilder to given SQL string equivalent
        */

    }, {
        key: 'getModelAttributes',
        value: function getModelAttributes() {
            var _this = this;
            // Get all property that has been set/changed during object life cycle
            var currentModelMembers = Object.getOwnPropertyNames(this);
            var getColumnsNameToSave = Stellar._.difference(currentModelMembers, this.initalModelMembers);

            // If nothing to save return false;
            if (!getColumnsNameToSave.length) {
                throw new Error('Model::getModelAttributes() trying to get model attribute on an empty model');
            }

            var columnValueMap = [];

            Stellar._.each(getColumnsNameToSave, function (columnName, key) {
                columnValueMap.push({ 'columnName': columnName, 'rowValue': _this[columnName] });
            });

            return columnValueMap;
        }
    }, {
        key: 'saveIgnore',
        value: function saveIgnore() {
            this.ignoreStatement = true;
            this.save();
        }
    }]);

    return Model;
}(QueryBuilder);

module.exports = Model;
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Router = function () {
    function Router() {
        _classCallCheck(this, Router);

        this.routeCollection = global.Stellar.Kernel.routesCollection = new (require('./routeCollection'))();
        this.recursiveReadDir = require('recursive-readdir');
        this.controllersExpressRouters = [];
        this.controllersExpressMiddlewares = [];
        this.controllersNames = [];
    }

    _createClass(Router, [{
        key: 'registerRoutes',
        value: function registerRoutes() {
            var _this = this;
            // Loop over routes folder to get all routes described by user
            this.recursiveReadDir(Stellar.Config.rootPath + 'routes', [this.preventBadRequire], function (err, files) {
                var _that = _this;
                // Inflate routeCollection object by iterating over route files and require them
                Stellar._.each(files, function (filePath, key) {
                    require(filePath);
                });
                // Start registering route described against express router
                var collection = _this.routeCollection.getCollection();
                // Iterate over routes
                Stellar._.each(collection, function (route, key) {
                    var routeObject = route[1];
                    // Instance of controller already exists
                    if (typeof _that.controllersExpressRouters[routeObject.controllerName] != 'undefined') {
                        var tmpController = _that.controllersExpressRouters[routeObject.controllerName].controller;
                        var tmpExpress = _that.controllersExpressRouters[routeObject.controllerName].express;
                        var tmpExpressRouter = _that.controllersExpressRouters[routeObject.controllerName].expressRouter;
                        // Instance doesnt exist yet, so make one
                    } else {
                            if (routeObject.isControllerNamespaced === true) {
                                //console.log(route.controllerNamespace);
                                var tmpController = new (require(_that.getControllerRequirePath(routeObject.controllerName, routeObject.controllerNamespace)))();
                            } else {
                                var tmpController = new (require(_that.getControllerRequirePath(routeObject.controllerName)))();
                            }

                            var tmpExpress = require('express');
                            var tmpExpressRouter = tmpExpress.Router();
                        }
                    // Check for middleware if any
                    if (typeof routeObject['middlewares'] !== 'undefined' && Array.isArray(routeObject.middlewares)) {

                        Stellar._.each(routeObject.middlewares, function (middlewareObj, key) {

                            if (middlewareObj.middlewareNamespace !== null) {
                                var tmpMiddleware = new (require(_that.getMiddlewareRequirePath(middlewareObj.middlewareName, middlewareObj.middlewareNamespace)))();
                            } else {
                                var tmpMiddleware = new (require(_that.getMiddlewareRequirePath(middlewareObj.middlewareName)))();
                            }
                            // Register middleware on express router
                            tmpExpressRouter.use(routeObject.URL, function (req, res, next) {
                                //tmpController[routeObject.methodAction](req, res);
                                tmpMiddleware[middlewareObj.middlewareMethod](req, res, next);
                            });
                        });
                    }
                    // Get HTTP methods described
                    var httpMethods = routeObject.httpMethods;
                    // Register route against express and call associated controller's method to give response
                    Stellar._.each(httpMethods, function (httpMethod, key) {
                        // Make sure we compare on same case
                        // @TODO: Missing methods, put, delete, ...
                        httpMethod = httpMethod.toUpperCase();
                        switch (httpMethod) {
                            case 'GET':
                                tmpExpressRouter.get(routeObject.URL, function (req, res) {
                                    tmpController[routeObject.methodAction](req, res);
                                });
                                break;
                            case 'POST':
                                tmpExpressRouter.post(routeObject.URL, function (req, res) {
                                    tmpController[routeObject.methodAction](req, res);
                                });
                                break;
                            case 'ALL':
                                tmpExpressRouter.all(routeObject.URL, function (req, res) {
                                    tmpController[routeObject.methodAction](req, res);
                                });
                                break;
                            default:
                                throw new Error(httpMethod + ' is not a valid HTTP METHOD (GET, POST, PUT, DELETE, ALL)');
                                break;
                        }
                    });
                    // Require file if nots already did previously
                    if (typeof _that.controllersExpressRouters[routeObject.controllerName] == 'undefined') {
                        _that.controllersExpressRouters[routeObject.controllerName] = {
                            controller: tmpController,
                            express: tmpExpress,
                            expressRouter: tmpExpressRouter
                        };
                        // Index controllers names
                        _that.controllersNames.push(routeObject.controllerName);
                    }
                    // End iterating on routes from RouteCollection
                });
                // Make express app use router we've created before
                Stellar._.each(_this.controllersNames, function (controllerName, key) {
                    Stellar.Kernel.app.use(_that.controllersExpressRouters[controllerName].expressRouter);
                });
            });
        }
    }, {
        key: 'getMiddlewareRequirePath',
        value: function getMiddlewareRequirePath(middlewareName, normalizedSubpath) {
            if (typeof normalizedSubpath != 'undefined') {
                return Stellar.Config.rootPath + 'middlewares/' + normalizedSubpath + middlewareName;
            } else {
                return Stellar.Config.rootPath + 'middlewares/' + middlewareName;
            }
        }
    }, {
        key: 'getControllerRequirePath',
        value: function getControllerRequirePath(controllerName, normalizedSubpath) {
            if (typeof normalizedSubpath != 'undefined') {
                return Stellar.Config.rootPath + 'controllers/' + normalizedSubpath + controllerName;
            } else {
                return Stellar.Config.rootPath + 'controllers/' + controllerName;
            }
        }
    }, {
        key: 'preventBadRequire',
        value: function preventBadRequire(file, stats) {
            // @TODO: Validate path given to avoid security breaks
            return false;
        }
    }]);

    return Router;
}();

module.exports = Router;
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RouteCollection = function () {
    function RouteCollection() {
        _classCallCheck(this, RouteCollection);

        this.routesCollection = [];
    }

    _createClass(RouteCollection, [{
        key: 'addRoute',
        value: function addRoute(route) {
            var routeObject = new (require('./route'))(route);
            this.routesCollection.push([route.name, routeObject]);
        }
    }, {
        key: 'addRoutes',
        value: function addRoutes(routes) {
            if (!Array.isArray(routes)) {
                throw new Error('RouteCollection.addRoutes() requires an array as argument "' + (typeof routes === 'undefined' ? 'undefined' : _typeof(routes)) + '" given');
            }
            var _this = this;
            Stellar._.each(routes, function (route, key) {
                _this.addRoute(route);
            });
        }
    }, {
        key: 'getCollection',
        value: function getCollection() {
            return this.routesCollection;
        }
    }, {
        key: 'setCollection',
        value: function setCollection(routeCollection) {
            this.routesCollection = routeCollection;
        }
    }]);

    return RouteCollection;
}();

;

module.exports = RouteCollection;
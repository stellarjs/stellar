"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Database = function () {
    function Database() {
        _classCallCheck(this, Database);

        this.initDatabaseMembers();
    }

    _createClass(Database, [{
        key: 'initDatabaseMembers',
        value: function initDatabaseMembers() {
            this.dbConnections = Stellar.Config.__db_connections;
            this.defaultConnectionName = null;
            this.mysql = null;
            this.dbConnectionsCollection = this.createDbConnectionsCollection();
        }

        //@TODO: Do the method noob !

    }, {
        key: 'withDatabase',
        value: function withDatabase(databaseName) {
            if (typeof databaseName !== 'string') {}

            return this;
        }
    }, {
        key: 'createDbConnectionsCollection',
        value: function createDbConnectionsCollection() {
            var collection = [];
            this.mysql = require('mysql');
            var _this = this;

            Stellar._.each(this.dbConnections, function (dbConnection, key) {
                if (dbConnection.hasOwnProperty('isDefault') === true && dbConnection.isDefault === true && _this.defaultConnectionName === null) {
                    // Set default connection
                    _this.defaultConnectionName = dbConnection.name;
                }

                var connection = _this.mysql.createConnection(dbConnection);
                connection.on('error', function (err) {
                    console.log(dbConnection.name + 'has failed to connect:');
                    console.log(err);
                    console.log(err.code); // 'ECONNREFUSED'
                    console.log(err.fatal); // true
                });

                connection.connect();
                collection[dbConnection.name] = connection;
            });
            return collection;
        }
    }, {
        key: 'executeQuery',
        value: function executeQuery() {
            console.log('Query to be executed', this.queryString);
            var _this = this;
            var connectionName = this.connectionName || this.defaultConnectionName;
            // Promise return val to be sure its synchronous
            return new Promise(function (resolve, reject) {
                _this.dbConnectionsCollection[connectionName].query(_this.queryString, function (err, rows, fields) {
                    if (err) {
                        reject(err);
                    }
                    resolve(rows);
                });
            });
        }
    }]);

    return Database;
}();

module.exports = Database;
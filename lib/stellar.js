"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Kernel = function () {
    function Kernel(rootPath) {
        _classCallCheck(this, Kernel);

        this.inflateKernel(rootPath);
    }

    _createClass(Kernel, [{
        key: 'inflateKernel',
        value: function inflateKernel(rootPath) {
            this.setGlobals(rootPath);
            this.setKernelMembers();
            this.expressSetup();
        }
    }, {
        key: 'setKernelMembers',
        value: function setKernelMembers() {
            this.express = require('express');
            Stellar.Kernel.app = this.express();
            this.templater = new (require('./templates/templater'))();
            this.router = new (require('./routes/router'))();
        }
    }, {
        key: 'setGlobals',
        value: function setGlobals(rootPath) {
            global.Stellar = {};
            global.Stellar.Kernel = {};
            global.Stellar.Config = require(rootPath + '/config/config');
            global.Stellar._ = require('underscore');

            Stellar.Config.rootPath = rootPath + '/';
            Stellar.Config.moduleRootPath = __dirname + '/';
            Stellar.Config.modelsPath = Stellar.Config.rootPath + 'models/';
            Stellar.Config.frameworkPath = Stellar.Config.rootPath + 'framework/';
            Stellar.Config.__cachePath = Stellar.Config.rootPath + Stellar.Config.__cachePath;
        }
    }, {
        key: 'expressSetup',
        value: function expressSetup() {
            var cookieParser = require('cookie-parser');
            var session = require('express-session');
            var bodyParser = require('body-parser');
            var path = require('path');
            var uuid = require('uuid');

            // Static content folder declaration
            if (typeof Stellar.Config.__assetsFolder.virtualURL != 'undefined') {
                Stellar.Kernel.app.use('/' + Stellar.Config.__assetsFolder.virtualURL, this.express.static(path.join(Stellar.Config.rootPath, Stellar.Config.__assetsFolder.publicDir)));
            } else {
                Stellar.Kernel.app.use(this.express.static(path.join(Stellar.Config.rootPath, Stellar.Config.__assetsFolder.publicDir)));
            }
            Stellar.Kernel.app.use(cookieParser());
            Stellar.Kernel.app.use(bodyParser.urlencoded({
                extended: true
            }));
            // Parse application/json
            Stellar.Kernel.app.use(bodyParser.json());
            Stellar.Kernel.app.use(session({
                genid: function genid(req) {
                    return uuid.v4(); // use UUIDs for session IDs
                },
                secret: Stellar.Config.__session.secret,
                secure: Stellar.Config.__session.secure,
                resave: false,
                saveUninitialized: false
            }));
        }
    }, {
        key: 'ignition',
        value: function ignition() {
            this.initializeTemplater();
            this.initializeRoutes();
            this.initializeWebServer();
        }
    }, {
        key: 'initializeTemplater',
        value: function initializeTemplater() {
            this.templater.registerFromConfig();
        }
    }, {
        key: 'initializeRoutes',
        value: function initializeRoutes() {
            this.router.registerRoutes();
        }
    }, {
        key: 'initializeWebServer',
        value: function initializeWebServer() {
            console.log('Server has started on port:', Stellar.Config.__port);
            Stellar.Kernel.app.listen(Stellar.Config.__port);
        }
    }]);

    return Kernel;
}();

module.exports = Kernel;
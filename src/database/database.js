"use strict";

class Database {

    constructor() {
        this.initDatabaseMembers()
    }

    initDatabaseMembers() {
        this.dbConnections = Stellar.Config.__db_connections;
        this.defaultConnectionName = null;
        this.mysql = null;
        this.dbConnectionsCollection = this.createDbConnectionsCollection();
    }

    //@TODO: Do the method noob !
    withDatabase(databaseName) {
        if (typeof databaseName !== 'string') {

        }

        return this;
    }

    createDbConnectionsCollection() {
        var collection = [];
        this.mysql = require('mysql');
        var _this = this;

        Stellar._.each(this.dbConnections, function(dbConnection, key) {
            if (dbConnection.hasOwnProperty('isDefault') === true &&
                dbConnection.isDefault === true &&
                _this.defaultConnectionName === null) {
                    // Set default connection
                    _this.defaultConnectionName = dbConnection.name;
            }

            var connection = _this.mysql.createConnection(dbConnection);
            connection.on('error', function(err) {
                console.log(dbConnection.name+'has failed to connect:')
                console.log(err);
                console.log(err.code); // 'ECONNREFUSED'
                console.log(err.fatal); // true
            });

            connection.connect();
            collection[dbConnection.name] = connection;
        });
        return collection;
    }

     executeQuery() {
         console.log('Query to be executed', this.queryString);
        var _this = this;
        var connectionName = this.connectionName || this.defaultConnectionName;
        // Promise return val to be sure its synchronous
        return new Promise(function(resolve, reject) {
            _this.dbConnectionsCollection[connectionName].query(_this.queryString, function(err, rows, fields) {
                if (err) {
                    reject(err);
                }
               resolve(rows);
           });
        });
    }

}

module.exports = Database;

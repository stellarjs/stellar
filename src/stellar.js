"use strict";

class Kernel {

    constructor(rootPath) {
        this.inflateKernel(rootPath);
    }

    inflateKernel(rootPath) {
        this.setGlobals(rootPath);
        this.setKernelMembers();
        this.expressSetup();
    }

    setKernelMembers() {
        this.express = require('express');
        Stellar.Kernel.app = this.express();
        this.templater = new (require('./templates/templater'))();
        this.router = new (require('./routes/router'))();
    }

    setGlobals(rootPath) {
        global.Stellar = {};
        global.Stellar.Kernel = {}
        global.Stellar.Config = require(rootPath + '/config/config');
        global.Stellar._ = require('underscore');

        Stellar.Config.rootPath = rootPath + '/';
        Stellar.Config.moduleRootPath = __dirname + '/';
        Stellar.Config.modelsPath = Stellar.Config.rootPath + 'models/';
        Stellar.Config.frameworkPath = Stellar.Config.rootPath + 'framework/';
        Stellar.Config.__cachePath = Stellar.Config.rootPath + Stellar.Config.__cachePath;
    }
    expressSetup() {
        var cookieParser = require('cookie-parser');
        var session = require('express-session');
        var bodyParser = require('body-parser');
        var path = require('path');
        var uuid = require('uuid');

        // Static content folder declaration
        if (typeof Stellar.Config.__assetsFolder.virtualURL != 'undefined') {
            Stellar.Kernel.app.use('/' + Stellar.Config.__assetsFolder.virtualURL, this.express.static(path.join(Stellar.Config.rootPath, Stellar.Config.__assetsFolder.publicDir)));
        } else {
            Stellar.Kernel.app.use(this.express.static(path.join(Stellar.Config.rootPath, Stellar.Config.__assetsFolder.publicDir)));
        }
        Stellar.Kernel.app.use(cookieParser());
        Stellar.Kernel.app.use(bodyParser.urlencoded({
          extended: true,
        }));
        // Parse application/json
        Stellar.Kernel.app.use(bodyParser.json());
        Stellar.Kernel.app.use(session({
          genid: function(req) {
            return uuid.v4(); // use UUIDs for session IDs
          },
          secret: Stellar.Config.__session.secret,
          secure: Stellar.Config.__session.secure,
          resave: false,
          saveUninitialized: false,
        }));
    }

    ignition() {
        this.initializeTemplater();
        this.initializeRoutes();
        this.initializeWebServer();
    }

    initializeTemplater() {
        this.templater.registerFromConfig();
    }

    initializeRoutes() {
        this.router.registerRoutes();
    }

    initializeWebServer() {
        console.log('Server has started on port:', Stellar.Config.__port);
        Stellar.Kernel.app.listen(Stellar.Config.__port);
    }
}

module.exports = Kernel;

"use strict";


class Route {

    constructor(routeObj) {
        this.setDefaults();
        this.routeDescription = routeObj;
        this.inflateRoute();
    }

    setDefaults() {
        this.isControllerNamespaced = false;
        this.hasMiddlewares = false;
        this.middlewares = [];
    }

    inflateRoute() {
        this.extractControllerData();
        this.extractHttpMethods();
        this.extractURL();
        this.extractMiddlewares();
    }

    extractURL() {
        this.URL = this.routeDescription.url;
    }

    extractMiddlewares() {
        var _this = this;
        if (typeof this.routeDescription.middlewares != 'undefined') {
            if (Array.isArray(this.routeDescription.middlewares)) {
                this.hasMiddlewares = true;
                var alreadyProcessed = [];
                Stellar._.each(this.routeDescription.middlewares, function(middlewareData, key) {
                    var middlewareName = null;
                    var middlewareNamespace = null;
                    var middlewareMethod = null;
                    // #1: Check for middleWare's method
                    var explodedMiddlewareData = middlewareData.split('@');
                    var leftMiddlewareData = explodedMiddlewareData[0];
                    var rightMiddlewareData = explodedMiddlewareData[1] || null;
                    var checkSubDirPathReg = new RegExp('\/|/', 'gi');
                    // #1: check for namespce
                    if (leftMiddlewareData.match(checkSubDirPathReg) !== null) {
                        var explodedLeftMiddlewareData = leftMiddlewareData.split('/');
                        // pop() is used to remove controllerName since its the last element of exploded namespace of explodedLeftControllerData
                        middlewareName = explodedLeftMiddlewareData.pop();
                        middlewareNamespace = explodedLeftMiddlewareData.join('/') + '/';
                    } else {
                        middlewareName = leftMiddlewareData;
                    }
                    if (Stellar._.contains(alreadyProcessed, middlewareName)) {
                        return;
                    } else {
                        alreadyProcessed.push(middlewareName);
                    }
                    // Check if middleware's methods is given or not
                    if (rightMiddlewareData !== null) {
                        middlewareMethod = rightMiddlewareData;
                    } else {
                        // If none default on process()
                        middlewareMethod = 'process';
                    }
                    /* Add this middleware to the list */
                    _this.middlewares.push({
                        middlewareName: middlewareName,
                        middlewareNamespace: middlewareNamespace,
                        middlewareMethod: middlewareMethod
                    });
                });
            } else {
                throw new Error('Route middlewares has to be an array "' + typeof this.routeDescription.middlewares + '" given');
            }

        }
    }

    extractHttpMethods() {
        if (!Array.isArray(this.routeDescription.httpMethods)) {
            throw new Error('Route HTTP methods has to be an array "' + typeof this.routeDescription.methods + '" given');
        }
        this.httpMethods = this.routeDescription.httpMethods;
    }

    extractControllerData() {
        var explodedControllerData = this.routeDescription.controller.split('@');
        if (explodedControllerData.length !== 2) {
            throw new Error(this.routeDescription.name + ' has invalid "controller" attribute');
        }
        var leftControllerData = explodedControllerData[0];
        var rightControllerData = explodedControllerData[1];
        var checkSubDirPathReg = new RegExp('\/|/', 'gi');
        var controllerPath = null;

        // Extract Controller name and path (if one present)
        if (leftControllerData.match(checkSubDirPathReg) !== null) {
            this.isControllerNamespaced = true;
            var explodedLeftControllerData = leftControllerData.split('/');
            // pop() is used to remove controllerName since its the last element of exploded namespace of explodedLeftControllerData
            var controllerName = explodedLeftControllerData.pop();
            this.controllerNamespace = explodedLeftControllerData.join('/') + '/';
            this.controllerName = controllerName;
        } else {
            this.controllerName = leftControllerData;
        }

        // Extract method to be executed on this route call
        this.methodAction = rightControllerData;
    }
}

module.exports = Route;

"use strict";

class RouteCollection {

    constructor() {
        this.routesCollection = [];
    }

    addRoute(route) {
        var routeObject = new (require('./route'))(route);
        this.routesCollection.push([route.name, routeObject]);
    }

    addRoutes(routes) {
        if (!Array.isArray(routes)) {
            throw new Error('RouteCollection.addRoutes() requires an array as argument "' + typeof routes + '" given')
        }
        var _this = this;
        Stellar._.each(routes, function(route, key){
            _this.addRoute(route);
        });
    }

    getCollection() {
        return this.routesCollection;
    }

    setCollection(routeCollection) {
        this.routesCollection = routeCollection;
    }

};

module.exports = RouteCollection;

"use strict";

var QueryBuilder = require('./../querybuilder/querybuilder');

class Model extends QueryBuilder {

    constructor() {
        super();
        this.initModelMembers();
        //@TODO: Save children config if any
        // this.loadCustomConfig();

        // Be sure its the last thing we init in this constructor
        this.initalModelMembers = Object.getOwnPropertyNames(this);
        this.initalModelMembers.push('initalModelMembers');
    }

    initModelMembers() {
        this.isNewObject = true;
        this.ignoreStatement = false;
        // Get Original Model Name
        this.table = this.constructor.name.toLowerCase();
        this.timestamp = false;
        this.primaryKey = 'id';
        this.guarded = [];
        this.fillable = [];
        this.connectionName = null;
        this.path = '';

        //@TODO: Segment each part of a possible Request (WHERE/UPDATE/FROM/JOIN/ORDER/GROUP/...) into independant var.
        // and once we have to execute query, just rebuild the full query depending on these variables
    }

     async all() {
        var result = await this.select('*').from(this.table).get();
        this.resetQueryString();
        return result;
    }

    save() {
        var query = null;
        var columnValueMap = this.getModelAttributes();
        var _this = this;

        if (this.ignoreStatement === true) {
            query = this.insertIgnoreIn(this.table);
        } else {
            query = this.insertIn(this.table);
        }

        var columnValueMapLength = columnValueMap.length - 1;

        Stellar._.each(columnValueMap, function(rowData, key){
            var  rowValue = _this.mysql.escape(rowData.rowValue);
            var colName = _this.mysql.escape(rowData.columnName).replace(/'/g, '');
            if (key == columnValueMapLength) {
                query.addRowAssignation(rowValue, colName, true);
            } else {
                query.addRowAssignation(rowValue, colName);
            }
        });

        query.executeQuery();
        this.resetQueryString();
    }

    // @TODO: Test + Rework
    async find(_id) {
        var result = await this.select('*').from(this.table).where(this.primaryKey, '=', _id).get();
        this.resetQueryString();
        // Return [0] since find should always return a single response, not a collection
        return result[0];
    }

     async get() {
        var queryResult = await this.executeQuery();
        return queryResult;
        //var finalResult = false;
        //var _this = this;
/*
        console.log('Just got result of query', new Date.now());

        if (queryResult.length > 1) {
            finalResult = [];
            Stellar._.each(queryResult, function(row, key){
                // Rebuild Original Object;
                var objectModel = _this.rebuildToNativeObject(row);
                finalResult.push(objectModel);
            });
        } else if (queryResult.length == 1) {
            finalResult = this.rebuildToNativeObject(queryResult[0]);
        } else {
            throw new Error('An error occured while fetching data from DB');
        }
        console.log('PROUT PROUT');
        console.log('FINAL RESULT', finalResult);
        return finalResult;*/
    }

    rebuildToNativeObject(row) {
        var finalObject = {};
        var finalObject = new (require(Stellar.Config.modelsPath + this.path + this.constructor.name))();
        Stellar._.each(row, function(prop, keyName){
            finalObject[keyName] = prop;
        });
        return finalObject;
    }

    /*
    * Interactions with DB methods goes through queryBuilder to given SQL string equivalent
    */
    getModelAttributes() {
        var _this = this;
        // Get all property that has been set/changed during object life cycle
        var currentModelMembers = Object.getOwnPropertyNames(this);
        var getColumnsNameToSave = Stellar._.difference(currentModelMembers, this.initalModelMembers);

        // If nothing to save return false;
        if (!getColumnsNameToSave.length) {
            throw new Error('Model::getModelAttributes() trying to get model attribute on an empty model');
        }

        var columnValueMap = [];

        Stellar._.each(getColumnsNameToSave, function(columnName, key){
            columnValueMap.push({'columnName': columnName, 'rowValue': _this[columnName]});
        });

        return columnValueMap;
    }

    saveIgnore() {
        this.ignoreStatement = true;
        this.save();
    }
}

module.exports = Model;

"use strict";

//@TODO: Rework/Rethink, not happy with that -.-

class Templater {

    constructor() {
        Stellar.Kernel.swig = require('swig');
        // Disable express cache to rely only on swig's one
        Stellar.Kernel.app.set('view cache', false);
        this.loadConfig();
        this.loadFilters();
    }

    loadFilters() {
        //@TODO: To be removed, for test purpose only ATM
        //Instead make something that load classes
        var mydump = function (arr,level) {
                    var dumped_text = "";
                    if(!level) level = 0;

                    var level_padding = "";
                    for(var j=0;j<level+1;j++) level_padding += "    ";

                    if(typeof(arr) == 'object') {
                        for(var item in arr) {
                            var value = arr[item];

                            if(typeof(value) == 'object') {
                                dumped_text += level_padding + "'" + item + "' ...\n";
                                dumped_text += mydump(value,level+1);
                            } else {
                                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                            }
                        }
                    } else {
                        dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
                    }
                    return dumped_text;
                };
        Stellar.Kernel.swig.setFilter('dump', mydump);
    }

    loadConfig() {
        Stellar.Kernel.swig.setDefaults({
          cache: Stellar.Config.__useCache,
          locals: Stellar.Config.__viewLocals,
        });
    }

    registerFromConfig() {
        Stellar.Kernel.app.engine('tpl', Stellar.Kernel.swig.renderFile);
        Stellar.Kernel.app.set('view engine', 'tpl');
        Stellar.Kernel.app.set('views', Stellar.Config.rootPath + 'views');
    }

}

module.exports = Templater;

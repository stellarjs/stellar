##Stellar CORE

This is the package containing all the core features of Stellar Javascript Framework.

##Licence

MIT

##Author

Thibaud Chauvière [email me](mailto://thibaud.chauviere@gmail.com)
